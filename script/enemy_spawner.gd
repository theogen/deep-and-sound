extends Node2D


const MIN_DISTANCE = 200
const MAX_DISTANCE = 500


var enemies_count = 5
var enemies_timer = 2


onready var camera = get_node("/root/world/train/camera")
onready var zombie_prefab = load("res://prefab/zombie.tscn")
onready var train = get_node("/root/world/train")

func _ready():
	randomize()


func start():
	$timer.wait_time = enemies_timer
	$timer.start()


func stop(wipe = true):
	$timer.stop()
	if wipe: wipe()


func wipe():
	for i in range(0, $container.get_child_count()):
			$container.get_child(i).queue_free()


func spawn():
	if $container.get_child_count() == enemies_count:
		return
	var min_pos = train.global_position.x + train.train_center - rand_range(MIN_DISTANCE, MAX_DISTANCE)
	var max_pos = train.global_position.x + train.train_center + rand_range(MIN_DISTANCE, MAX_DISTANCE)
	var zombie = zombie_prefab.instance()
	$container.add_child(zombie)
	zombie.position.y = 103
	var left = randi() % 2 == 0
	zombie.position.x = min_pos if left else max_pos
	if left:
		print("Spawned 1 zombie on the left")
	else:
		print("Spawned 1 zombie on the right")


func _on_timer_timeout():
	spawn()
