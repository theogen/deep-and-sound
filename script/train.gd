extends Node2D


const ACCELERATION_RATE = 15
const DECELERATION_RATE = 8

const CART_PX_LENGTH = 23
const CART_POS_THRESHOLD = 10


onready var cart_prefab = load("res://prefab/cart.tscn")
onready var carts_parent = get_node("carts")
onready var background = get_node("/root/world/background")


var min_limit
var min_limit_hit = false
var max_limit = 10000
var max_limit_hit = false


var moving = false
var speed = 0
var hit_speed = 20
var max_speed = 50
var direction = Vector2.RIGHT

var train_center: int


func _ready():
	pass


func init(carts: int):
	for i in range(0, carts_parent.get_child_count()):
		carts_parent.get_child(i).queue_free();
	var last
	for i in range(0, carts):
		var cart = cart_prefab.instance()
		carts_parent.add_child(cart)
		cart.position.x = CART_PX_LENGTH * i;
		last = i
	var draisine_right = get_node("draisine_right")
	draisine_right.position.x = (last + 1) * CART_PX_LENGTH
	draisine_right.get_node("sprite").frame = 6
	train_center = last * CART_PX_LENGTH / 2
	$camera.position.x = train_center
	$carts_area.position.x = $camera.position.x
	$carts_area/shape.scale.x = (last + 1) * CART_PX_LENGTH / 2
	min_limit = get_node("/root/world/wall_left").position.x


func cart_at(x: float):
	for i in range(0, carts_parent.get_child_count()):
		var cart = carts_parent.get_child(i)
		if abs(cart.global_position.x - x) <= CART_POS_THRESHOLD:
			return cart
	return null


func filled():
	for i in range(0, carts_parent.get_child_count()):
		var cart = carts_parent.get_child(i)
		if cart.filled != cart.capacity:
			return false
	return true


func _on_carts_area_body_entered(body):
	for i in range(0, carts_parent.get_child_count()):
		carts_parent.get_child(i).fade(true)


func _on_carts_area_body_exited(body):
	for i in range(0, carts_parent.get_child_count()):
		carts_parent.get_child(i).fade(false)


func _physics_process(delta):
	$sound.volume_db = linear2db(speed / max_speed)
	if moving:
		speed += ACCELERATION_RATE * delta
	elif speed > 0:
		speed -= DECELERATION_RATE * delta
	elif speed < 0:
		speed = 0
	if speed > hit_speed:
		get_node("draisine_right/area").collision_layer = 0b00000000000000000011
		get_node("draisine_left/area").collision_layer = 0b00000000000000000011
	else:
		get_node("draisine_right/area").collision_layer = 0b00000000000000000001
		get_node("draisine_left/area").collision_layer = 0b00000000000000000001
	if abs(speed) > max_speed:
		speed = sign(speed) * max_speed
	if get_node("draisine_left").global_position.x - 17 <= min_limit and direction.x < 0:
		if not min_limit_hit:
			min_limit_hit = true
			$hit.play()
			return
		position.x = int(position.x)
		speed = 0
		return
	else:
		min_limit_hit = false
	if get_node("draisine_right").global_position.x + 19 >= max_limit and direction.x > 0:
		if not max_limit_hit:
			max_limit_hit = true
			get_node("/root/world").on_train_right_limit_hit()
			$hit.play()
		position.x = int(position.x)
		speed = 0
		return
	else:
		max_limit_hit = false
	position = position.move_toward(position + direction, delta * speed)
