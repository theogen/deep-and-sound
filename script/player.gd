extends KinematicBody2D

signal on_jump

var can_move = true
var stop_physics = false

var snap = false

var run_speed = 50
var jump_speed = -140
var gravity = 400

var velocity = Vector2()
var tween_position_x
var tween_position_y

var startx

var sprite
var eyelids

var idle_name = "idle"

var draisine = null

const JUMP_BUFFER = 0.3
var _jump_buffer = 0.0

onready var mining = get_node("/root/world/mining")
onready var train = get_node("/root/world/train")

func _ready():
	sprite = $sprite
	eyelids = get_node("eyelids")
	blink(true)
	align_position_to_pixels()
	startx = position.x
	#set_active(false)


func is_driving():
	return draisine != null and train.speed > 0

func set_active(state):
	can_move = state
	visible = state


func blink(state):
	if !state:
		eyelids.stop()
		get_node("eyelid").visible = false
		get_node("eyelid2").visible = false
	else:
		eyelids.play("eyelids")


func align_position_to_pixels():
	position = Vector2(round(position.x), position.y)


func flip(state):
	transform.x = Vector2(-1 if state else 1, 0)


func get_input():
	if not can_move:
		velocity.x = 0
		#sprite.play(idle_name)
		#blink(true)
		#align_position_to_pixels()
		return
	velocity.x = 0
	var right = Input.is_action_pressed('right')
	var left = Input.is_action_pressed('left')
	#var jump = Input.is_action_just_pressed('jump')
	var jump = false
	
	if not is_on_floor() and jump:
		_jump_buffer = JUMP_BUFFER
	
	if is_on_floor() and (jump or _jump_buffer > 0):
		velocity.y = jump_speed
		_jump_buffer = 0
		emit_signal("on_jump")
	
	if right:
		if is_on_floor():
			sprite.play("run")
		#blink(false)
		flip(false)
		velocity.x += run_speed
	if left:
		if is_on_floor():
			sprite.play("run")
		#blink(false)
		flip(true)
		velocity.x -= run_speed
	if !right and !left:
		if is_on_floor() and sprite.animation != "mine":
			sprite.play("idle")
		blink(true)
		#flip(false)
		#align_position_to_pixels()
	
	if !is_on_floor():
		if !right and !left:
			sprite.play("jump")
		#blink(false)
	if right or left:
		#blink(false)
		if !is_on_floor() and not "jump" in sprite.animation:
				sprite.play("jump")

	if draisine != null:
		if Input.is_action_pressed("drive"):
			draisine.drive(true)
			snap = true
		else:
			snap = false
			draisine.drive(false)
	
	if Input.is_action_just_pressed("mine"):
			if mining.start():
				sprite.play("mine")

func _physics_process(delta):
	if stop_physics:
		position = Vector2(tween_position_x, tween_position_y)
		return
	velocity += get_floor_velocity()
	velocity.y += gravity * delta
	get_input()
	if not snap:
		velocity = move_and_slide(velocity, Vector2(0, -1))
	else:
		velocity = move_and_slide_with_snap(velocity, Vector2(0, 8), Vector2(0, -1))
	if _jump_buffer > 0:
		_jump_buffer -= delta


func _on_animation_finished():
	if sprite.animation == "mine":
		sprite.play("idle")
		mining.end()


func die():
	print("Dead")
	get_node("/root/world").on_die()
	tween_position_x = position.x
	tween_position_y = position.y
	$sprite.play("jump")
	$tween.interpolate_property(self, "tween_position_y", position.y, position.y - 10, 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$tween.interpolate_property(self, "tween_position_y", position.y - 10, 160, 0.5, Tween.TRANS_QUAD, Tween.EASE_IN, 0.1)
	$tween.set_active(true)
