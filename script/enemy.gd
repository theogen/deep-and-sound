extends KinematicBody2D


const DISPAWN_DISTANCE = 700


var speed = 10
var gravity = 400


var velocity: Vector2

var stop_physics = false

var x = 0
var y = 0


onready var player = get_node("/root/world/player")


func _ready():
	pass


func ram(from_pos: Vector2):
	$audio.play()
	$shape.set_deferred("disabled", true)
	stop_physics = true
	var pos = global_position
	var dir = from_pos.direction_to(global_position)
	y = pos.y
	$tween.interpolate_property(self, "x", pos.x, pos.x + dir.x * 100, 0.7, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$tween.interpolate_property(self, "y", pos.y, 80, 0.5, Tween.TRANS_QUAD, Tween.EASE_OUT)
	$tween.interpolate_property(self, "y", 90, 160, 0.5, Tween.TRANS_QUAD, Tween.EASE_IN, 0.1)
	$tween.interpolate_property(self, "rotation_degrees", 0, 360, 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$tween.set_active(true)
	$sprite.play("fly")


func _physics_process(delta):
	if stop_physics:
		global_position = Vector2(x, y)
		if global_position.y == 160:
			queue_free()
		return
	if global_position.distance_to(player.global_position) > DISPAWN_DISTANCE:
		print("Zombie dispawned due to large distance")
		queue_free()
	velocity = Vector2.ZERO
	var dir = global_position.direction_to(player.global_position)
	velocity.x += dir.x * speed
	flip(dir.x < 0)
	#velocity += get_floor_velocity()
	#velocity.y += gravity * delta
	if player.global_position.y > 120:
		$sprite.play("idle")
		velocity = Vector2.ZERO
	velocity = move_and_slide(velocity, Vector2(0, -1))


func flip(state):
	transform.x = Vector2(-1 if state else 1, 0)


func _on_area_body_entered(body):
	if body.is_in_group("player") and not player.get_node("shape").disabled:
		$audio.play()
		player.die()
