extends Node


const ORE_COLORS = {
	"g" : Color("b37d11"),
	"r" : Color("971b1b"),
	"e" : Color("159b23"),
	"d" : Color("9fdfd5")
}

const PARTICLE_LIFETIME = {
	2 : 1.3,  3 : 1.2, 4 : 1.15, 5 : 1.1,
	6 : 1.04, 7 : .95, 8 : .87,  9 : .78, 10 : .68, 11 : .55
}


onready var player = get_node("/root/world/player")
onready var train = get_node("/root/world/train")
onready var column = get_node("/root/world/column_tracker")
onready var background = get_node("/root/world/background")
onready var particles = get_node("/root/world/ore_particles")


var ore_pos


func _ready():
	pass


func start():
	if player.is_driving():
		return false
	if column.ore_pos != null:
		ore_pos = column.ore_pos
		return true
	return false


func end():
	var type = background.mine_ore(ore_pos)
	column.update()
	var cart = train.cart_at(_cell_to_world_pos(ore_pos).x)
	if cart == null or (cart.ore != "" and cart.ore != type):
		_emit_paritcle(ore_pos, type, false)
		return
	if cart.ore == "":
		cart.set_ore(type, ORE_COLORS[type])
	_emit_paritcle(ore_pos, type, cart.deposit(1, PARTICLE_LIFETIME[int(ore_pos.y)]))
	if train.filled():
		get_node("/root/world").on_all_carts_filled()


func _emit_paritcle(pos, type, collect):
	if type == "g":
		get_node("/root/world/audio/coins").play()
	else:
		get_node("/root/world/audio/crystal").play()
	var particle
	for child in particles.get_children():
		if not child.emitting:
			particle = child
			break;
	particle.emitting = true
	particle.position = _cell_to_world_pos(pos)
	particle.color = ORE_COLORS[type]
	particle.z_index = 0 if collect else 5
	particle.lifetime = PARTICLE_LIFETIME[int(pos.y)] if collect else 2
	particle.restart()


func _cell_to_world_pos(pos):
	return pos * 8 + Vector2(4, 4)
