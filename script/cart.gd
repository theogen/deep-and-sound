extends Area2D


const MAX_ORE_SCALE = 10


var ore: String = ""
var filled = 0
var capacity = 5


func _ready():
	pass


func set_ore(type: String, color: Color):
	ore = type
	$ore.modulate = color


func deposit(amount: int, wait: float):
	if filled + amount > capacity:
		return false
	filled += amount
	$timer.wait_time = wait
	$timer.start()
	return true


func fill(percentage: float):
	var scale = int(MAX_ORE_SCALE * percentage)
	$ore.scale.y = scale
	if scale == 0:
		$ore.scale.y = 1


func fade(state):
	if state:
		$animation.play("fade")
	else:
		$animation.play_backwards("fade")


func _on_body_entered(_body):
	fade(true)


func _on_body_exited(_body):
	fade(false)


func _on_timer_timeout():
	fill(float(filled) / capacity)
