extends Node


onready var player = get_node("/root/world/player")
onready var background = get_node("/root/world/background")


var previous_player_x: int = -1
var ore_pos

func _ready():
	pass


func update():
	previous_player_x = -1


func _process(_delta):
	if player.is_driving():
		background.highlight(previous_player_x, false)
		return false
	var pos = int(player.position.x / 8)
	if pos != previous_player_x:
		_on_new_position(previous_player_x, pos)
		previous_player_x = pos


func _on_new_position(prev, new):
	background.highlight(prev, false)
	ore_pos = background.highlight(new, true)
