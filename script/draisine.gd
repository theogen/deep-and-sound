extends KinematicBody2D

export var flip_direction = false


var train
var player_inside = false
var direction = Vector2.RIGHT
var driving = false


# Called when the node enters the scene tree for the first time.
func _ready():
	if flip_direction:
		direction = -direction;
	train = get_node("..")



func drive(state):
	if driving == state:
		return
	if state: $sprite.play("default")
	else: $sprite.stop()
	train.moving = state
	train.direction = direction
	driving = state


func _on_area_body_entered(body):
	if body.is_in_group("player"):
		player_inside = true
		body.draisine = self
	if body.is_in_group("enemy"):
		body.ram(global_position)


func _on_area_body_exited(body):
	if body.is_in_group("player"):
		player_inside = false
		body.draisine = null
		drive(false)
