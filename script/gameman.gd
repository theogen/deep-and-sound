extends Node2D


const LEVELS = [
	{
		length = 96,
		ore_rarity = 10,
		ores = 1,
		carts = 1,
		enemy_count = 0,
		enemy_timer = 3,
	},
	{
		length = 128,
		ore_rarity = 10,
		ores = 2,
		carts = 2,
		enemy_count = 5,
		enemy_timer = 2,
	},
	{
		length = 196,
		ore_rarity = 10,
		ores = 3,
		carts = 3,
		enemy_count = 7,
		enemy_timer = 2,
	},
	{
		length = 256,
		ore_rarity = 10,
		ores = 4,
		carts = 4,
		enemy_count = 10,
		enemy_timer = 1.5,
	}
]


onready var background = get_node("background")
onready var player = get_node("player")
onready var train = get_node("train")
onready var camera = get_node("train/camera")
onready var enemies = get_node("enemies")
onready var gui = get_node("gui")


var title = true
var restart = false
var level_end = false
var not_enough = false


var default_player_pos: Vector2
var default_train_pos: Vector2

var level_id = 0
var level


func _ready():
	level = LEVELS[level_id]
	background.ore_rarity = level.ore_rarity
	background.generate(level.length, level.ores)
	player.can_move = false
	default_player_pos = player.global_position
	default_train_pos = train.global_position
	train.init(level.carts)


func _process(_delta):
	if (title or restart) and Input.is_action_just_pressed("ui_accept"):
		start(title)
		title = false
		restart = false
	if not_enough and Input.is_action_just_pressed("ui_accept"):
		gui.get_node("not_enough").visible = false
		not_enough = false
	if level_end and Input.is_action_just_pressed("ui_accept"):
		gui.get_node("level_end").visible = false
		level_end = false
		level_id += 1
		if level_id < LEVELS.size():
			level = LEVELS[level_id]
			set_camera_parent(train)
			start(false)
		else:
			gui.get_node("win").visible = true
	if train.position.x + train.train_center >= background.end_x and not level_end:
		level_end = true
		on_level_end()


func start(first_start = false):
	gui.get_node("level/label").text = "Level " + str(level_id + 1)
	gui.get_node("level/fade").play("fade")
	gui.get_node("title").visible = false
	gui.get_node("restart").visible = false
	player.can_move = true
	player.stop_physics = false
	player.get_node("shape").disabled = false
	train.moving = false
	train.speed = 0
	if not first_start:
		player.get_node("tween").set_active(false)
		player.global_position = default_player_pos
		train.global_position = default_train_pos
		background.clear()
		background.generate(level.length, level.ores)
		enemies.wipe()
		train.init(level.carts)
	get_node("wall_right").visible = true
	get_node("wall_right/shape").disabled = false
	train.max_limit = background.end_x
	enemies.enemies_count = level.enemy_count
	enemies.enemies_timer = level.enemy_timer
	enemies.start()


func on_die():
	player.stop_physics = true
	player.get_node("shape").set_deferred("disabled", true)
	gui.get_node("restart").visible = true
	gui.get_node("not_enough").visible = false
	restart = true


func on_train_right_limit_hit():
	gui.get_node("not_enough").visible = true
	not_enough = true


func on_all_carts_filled():
	train.max_limit = 100000
	get_node("wall_right").visible = false
	get_node("wall_right/shape").disabled = true


func on_level_end():
	# Unparenting camera
	set_camera_parent(self)
	# Locking player
	player.can_move = false
	player.get_node("sprite").play("idle")
	# Locking train
	train.moving = true
	# Stopping monsters
	enemies.stop()
	gui.get_node("level_end").visible = true


func set_camera_parent(parent):
	var pos = camera.global_position
	camera.get_node("..").remove_child(camera)
	parent.add_child(camera)
	camera.global_position = pos
