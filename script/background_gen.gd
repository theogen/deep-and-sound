extends TileMap


const FLOOR_HEIGHT = 13
const MIN_HEIGHT = 3
const MAX_HEIGHT = 10
const NO_ORE_LAYERS = 2

const STONE_IDS = [3, 4]
const GOLD_IDS = [5, 9, 13]
const RUBY_IDS = [6, 10, 14]
const EMERALD_IDS = [7, 11, 15]
const DIAMOND_IDS = [8, 12, 16]

const END_ID = 18

var ore_rarity = 10

var ore_max_tier = DIAMOND_IDS[0]
var size

var end_x: int = 100000


# Called when the node enters the scene tree for the first time.
func _ready():
	pass


func generate(length, ores):
	size = length
	for x in range(0, length):
		_build_column(x, ores)
		_build_stalactites(x)
	for x in range(length, length + 48):
		if x % 2 == 0:
			for y in range(0, FLOOR_HEIGHT + 1):
				set_cell(x, y, END_ID)
	end_x = size * 8
	get_node("/root/world/wall_right").position.x = end_x - 4


func clear():
	if size == null:
		return
	for x in range(0, size):
		for y in range(0, FLOOR_HEIGHT + 1):
			set_cell(x, y, -1)


func mine_ore(pos):
	var cell = get_cellv(pos)
	set_cellv(pos, STONE_IDS[0])
	if cell in GOLD_IDS:
		return "g"
	if cell in RUBY_IDS:
		return "r"
	if cell in EMERALD_IDS:
		return "e"
	if cell in DIAMOND_IDS:
		return "d"


func _build_column(x, ores):
	var id: int
	for y in range(0, rand_range(MIN_HEIGHT, MAX_HEIGHT)):
		id = STONE_IDS[0]
		if y >= NO_ORE_LAYERS and int(rand_range(0, ore_rarity)) == 0:
			id = int(rand_range(GOLD_IDS[0], GOLD_IDS[0] + ores))
		set_cell(x, FLOOR_HEIGHT - y, id)


func _build_stalactites(x):
	for y in range(0, rand_range(0, 5)):
		set_cell(x, y, STONE_IDS[0])


func highlight(x, state):
	var cell
	for y in range(0, MAX_HEIGHT):
		cell = get_cell(x, FLOOR_HEIGHT - y)
		if cell == INVALID_CELL:
			continue
		var vec = Vector2(x, FLOOR_HEIGHT - y);
		if highlight_cell(cell, x, y, GOLD_IDS, state):
			return vec
		if highlight_cell(cell, x, y, RUBY_IDS, state):
			return vec
		if highlight_cell(cell, x, y, EMERALD_IDS, state):
			return vec
		if highlight_cell(cell, x, y, DIAMOND_IDS, state):
			return vec
	return null

func highlight_cell(cell, x, y, type, state):
		if cell in type:
			set_cell(x, FLOOR_HEIGHT - y, type[1 if state else 0])
			return true
		return false
