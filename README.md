![](./texture/media/banner.png)

You are just a miner who tries to make a living. In a cave with zombies. And
lots of treasures.

## Hints/Controls

**Down Arrow** while standing on a draisine (a cart with a lever) will move the
whole train in the opposite direction.

**Up Arrow** while below an ore will mine it. A cart must be present to collect
it.

The only way to kill the **zombies** is to **run them over** with the
minecarts!

You have to **fill all the carts** with any ore (but you can't mix them!) to
proceed to the next level.

## Credits

See [CREDITS.md](./CREDITS.md).
