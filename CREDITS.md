# Credits

Made with [Godot](https://godotengine.org/).

Font: [Press Start 2P](https://fonts.google.com/specimen/Press+Start+2P) by [CodeMan32](http://zone38.net/).

Music: [All of Us](https://ericskiff.com/music/Resistor%20Anthems/04%20All%20of%20Us.mp3) ([Resistor Anthems](https://ericskiff.com/music/)) by [Eric Skiff](https://ericskiff.com/).

Cart sound: [steam train](https://freesound.org/people/inchadney/sounds/188059/) by [inchadney](https://freesound.org/people/inchadney/).

Cart impact sound: [CartImpact4](https://freesound.org/people/FFeller/sounds/532871/) by [FFeller](https://freesound.org/people/FFeller/).

Zombie sound: [Zombie gargles](https://freesound.org/people/Breviceps/sounds/445983/) by [Breviceps](https://freesound.org/people/Breviceps/).

Crystal break sound: [break-wine-glass](https://freesound.org/people/BristolStories/sounds/65902/) by [BristolStories](https://freesound.org/people/BristolStories/).
